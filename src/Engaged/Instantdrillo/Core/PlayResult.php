<?php
/*
 Copyright (c) 2013 Engaged srl <info@engaged.it>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace Engaged\Instantdrillo\Core;

use Engaged\Instantdrillo\Common\StringUtils;

class PlayResult
{

	const RESULT_CODE_WIN = "1";
	const RESULT_CODE_LOSE = "2";
	const RESULT_CODE_ALREADY_PLAYED_FOR_TODAY = "3";
	const RESULT_CODE_ALREADY_WINNED = "4";
	
	
	const WIN = "win";
	const PRIZE = "prize";
	const RESULT_CODE = "resultCode";
	const RESULT = "result";

	private $win;
	private $prize;
	private $resultCode;
	private $result;
	
	private $json;

	public function __construct($json=null)
    {
		$this->win=null;
		$this->prize=null;

		$this->json = $json;
		
		if ($json==null)
			return;

		$data = StringUtils::jsonToArray($json);

		if (strtolower($data[PlayResult::WIN])=="false" || $data[PlayResult::WIN]=="0")
			$this->win = false;
		if (strtolower($data[PlayResult::WIN])=="true" || $data[PlayResult::WIN]=="1")
			$this->win = true;

		if ($this->win) {
			$this->prize = new Prize();
			$this->prize->initFromArray($data[PlayResult::PRIZE]);
		}
		
		$this->resultCode=$data[PlayResult::RESULT_CODE];
		$this->result=$data[PlayResult::RESULT];
	}


	/**
	 * @return boolean
	 */
	public function isWin()
    {
		return $this->win;
	}

	/**
	 * @return Prize
	 */
	public function getPrize()
    {
		return $this->prize;
	}

	public function getResult() {
		return $this->result;
	}
	
	public function getResultCode() {
		return $this->resultCode;
	}
	
	
	/**
	 * @return string
	 */
	public function getJson() {
		return $this->json;
	}

}
