<?php
namespace Engaged\Instantdrillo\Tests;

use Engaged\Instantdrillo\Constants;

use Engaged\Instantdrillo\Exception\ValidationException;

use Engaged\Instantdrillo\Instantdrillo;

class InstantdrilloTest extends AbstractTest
{


	private $__baseUrl   = 'http://localhost:8080/engaged-server/api/instantdrillo/1.0';
	//private $__baseUrl   = 'http://instantdrillo.engaged.it/api/1.0/';
	//private $__baseUrl   = 'http://www.instantdrillo.com/api/1.0/';
	private $__gameToken = 'fd6b8846ace63971bfb76f3f1b20b5de';

	public function testWithWrongUrlForConnectionProblem()
    {
		$haveException = false;

		$instantdrillo = new Instantdrillo("ThisIsAWrongUrl" , null);
		try {
			$instantdrillo->play("fake");
		} catch (\Exception $e) {
			$haveException=true;
		}

		$this->assertTrue($haveException);
	}


	public function testWithWrongGameToken()
    {

		$haveWrongGameTokenException = false;

		$instantdrillo = new Instantdrillo($this->__baseUrl , "wrongGameToken");

		//$instantdrillo->setGameToken("wrongGameToken");

		try {

			$instantdrillo->play("fake");

		} catch (ValidationException $validationException) {

			$errors = $validationException->getErrors();
			foreach ($errors as $error) {
				if ($error->getErrorCode()==Constants::VALIDATION_ERROR_CODE_WRONG_GAME_TOKEN)
					$haveWrongGameTokenException=true;

			}

		} catch (\Exception $e) {

			echo "vacca la bindella, another exception occoured!\n";
			echo "error message : " . $e->getMessage();
		}

		$this->assertTrue($haveWrongGameTokenException);
	}


	public function testWithGoodData()
    {
		$alreadyWon = false;
		$instantdrillo = new Instantdrillo($this->__baseUrl, $this->__gameToken);

		$result = null;

        try {

        	$result = $instantdrillo->play("aUserToken");

        } catch (ValidationException $e) {
			$errors = $e->getErrors();
			echo "validation errors: ".$e->getJson();
						
		}

		$this->assertNotNull(($result->isWin()!=null && ($result->isWin()==true || $result->isWin()==false)));

	}

	
	
	
}
